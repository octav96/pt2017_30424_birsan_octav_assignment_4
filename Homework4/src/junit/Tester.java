package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import bank.Account;
import bank.Bank;
import bank.Person;

public class Tester {

	@Test
	public void testBankPersonAddition() {
		Bank b=new Bank();
		b.addPerson(new Person("Octav"));
		b.addPerson(new Person("Octi"));
		b.addPerson(new Person("Octy"));
		assertEquals(b.getClients().size(),3);
	}
	@Test
	public void testBankPersonRemoval() {
		Bank b=new Bank();
		b.addPerson(new Person("Octav"));
		b.addPerson(new Person("Octi"));
		b.addPerson(new Person("Octy"));
		b.removePerson(new Person("Octi"));
		assertEquals(b.getClients().size(),2);
	}
	@Test
	public void testBankAccountAddition() {
		Bank b=new Bank();
		Account a=new Account(1);
		Account a1=new Account(2);
		Person p=new Person("Octav");
		b.addPerson(p);
		b.addAccount(p, a);
		b.addAccount(p, a1);
		assertEquals(b.getClients().get(p.getName()).getAccounts().size(),2);
	}
	@Test
	public void testBankAccountRemoval() {
		Bank b=new Bank();
		Account a=new Account(1);
		Account a1=new Account(2);
		Person p=new Person("Octav");
		b.addPerson(p);
		b.addAccount(p, a);
		b.addAccount(p, a1);
		b.removeAccount(p, a1);
		assertEquals(b.getClients().get(p.getName()).getAccounts().size(),1);
	}

}
