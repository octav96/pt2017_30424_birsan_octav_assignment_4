package bank;

public class SpendingAccount extends Account{

	public SpendingAccount() {
		super(1);
	}

	public void withdrawMoney(int money){
		this.setMoney(this.getMoney()-money);
		setChanged();
		notifyObservers("Money was withdrawn from the spending account with the ID: " + this.getId());
	}
	public void addMoney(int money){
		this.setMoney(this.getMoney()+money);
		setChanged();
		notifyObservers("Money was added into the spending account with the ID: " + this.getId());
	}
}
