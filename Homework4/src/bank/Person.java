package bank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable, Observer{
		private String name;
		private List<Account> accounts;
		public Person(String name){
			this.name=name;
			accounts=new ArrayList<Account>();
		}
		//returns the name of a person
		public String getName(){
			return this.name;
		}
		//returns the list of accounts of the person
		public List<Account> getAccounts(){
			return this.accounts;
		}
		//adds an account to a person
		public void addAccount(Account a){
			accounts.add(a);
		}
		//observer notifier
		
		public void update(Observable a, Object whatHappened) {
			System.out.println(whatHappened.toString());
			
		}
		
}
