package bank;

public interface BankProc {
	void addPerson(Person p);
	void removePerson(Person p);
	void addAccount(Person p,Account a);
	void removeAccount(Person p,Account a);
	String generateReport();
}
