package bank;

public class SavingAccount extends Account{
	private int added=0;
	private int withdrawn=0;
	private int percentage=8;
	//private int depositingTime;
	private int moneyReturned=0;
	public SavingAccount() {
		super(0);
		//this.depositingTime=0;
	}
	public void addMoney(int money){
		if(this.added==0){
			this.setMoney(this.getMoney()+money);
			this.added=1;
			setChanged();
			notifyObservers("Money was added into the saving account with the ID: " + this.getId());
		}
	}
	public void withdrawMoney(){
		if(this.withdrawn==0){
			this.moneyReturned=(this.getMoney()+this.getMoney()*8/100);
			this.setMoney(0);
			this.withdrawn=1;
			setChanged();
			notifyObservers("Money was withdrawn in the saving account with the ID: " + this.getId());
		}
	}
	public int getMoneyReturned(){
		return this.moneyReturned;
	}
}
