package bank;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map.Entry;

public class Bank implements BankProc,Serializable{
	private Hashtable<String, Person> clients;
	private String report;
	public Bank(){
		this.clients=new Hashtable<String,Person>();
		this.report=new String();
	}
	//adds a person to the bank
	public void addPerson(Person p){
		assert isWellFormed();
		assert p!=null;
		assert this.clients.containsKey(p.getName())==false;
		this.clients.put(p.getName(), p);
		assert this.clients.containsKey(p.getName())==true;
		assert isWellFormed();
	}
	//removes a person from the bank
	public void removePerson(Person p){
		assert isWellFormed();
		assert p!=null;
		assert this.clients.containsKey(p.getName())==true;
		this.clients.remove(p.getName());
		assert this.clients.containsKey(p.getName())==false;
		assert isWellFormed();
	}
	//and an account to a specifie person
	public void addAccount(Person p,Account a){
		assert isWellFormed();
		assert p!=null;
		assert this.clients.containsKey(p.getName())==true;
		assert this.clients.get(p.getName()).getAccounts().contains(a)==false;
		//assert 1!=0==true;
		//assert this.clients.get(p.getName()).getAccounts().contains(a)==false;
		this.clients.get(p.getName()).addAccount(a);
		assert this.clients.get(p.getName()).getAccounts().contains(a)==true;
		assert isWellFormed();
	}
	//removes an account of a  specified person from the bank
	public void removeAccount(Person p,Account a){
		assert isWellFormed();
		assert p!=null;
		assert this.clients.containsKey(p.getName())==true;
		assert this.clients.get(p.getName()).getAccounts().contains(a)==true;
		this.clients.get(p.getName()).getAccounts().remove(a);
		assert this.clients.get(p.getName()).getAccounts().contains(a)==false;
		assert isWellFormed();
	}
	public String generateReport(){
			//return "The bank currently has";
		String ret=new String(); 
			//clients.
		int clientNr=0;
		int accountNr=0;
		int money=0;
			for(String s:this.clients.keySet()){
				clientNr+=1;
			}
			String saux;//=5 == 3 ? 5 : 3;
			ret=ret + "The bank has " + clientNr + (saux = clientNr == 1 ? " client" : " clients");
			for(String s:this.clients.keySet()){
				accountNr+=clients.get(s).getAccounts().size();
			}
			ret=ret+" and a total of "+accountNr +" accounts";
			for(String s:this.clients.keySet()){
				for(Account a:clients.get(s).getAccounts()){
					money+=a.getMoney();
				}
			}
			ret=ret+ " depositing a total amount of " + money + " EURO.";
			return ret;
			
	}
	public void setClients(Hashtable<String, Person> clients){
		this.clients=clients;
	}
	public Hashtable<String, Person> getClients(){
		return this.clients;
	}
	public boolean isWellFormed(){
		for(Entry<String, Person> entry:clients.entrySet()){
			if(entry.equals(null)){
				return false;
			}
			if(entry.getKey().equals(null))
				return false;
		}
		return true;
	}
}
