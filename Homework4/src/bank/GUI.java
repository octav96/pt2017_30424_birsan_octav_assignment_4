package bank;

//package bank;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

//import gui.Bank;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Hashtable;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private Bank b=new Bank();
	private String personName=new String();
	private Person p;
	private int accountId=-1;
	private JTextField textField_3;
	private AccountWindow accw= new AccountWindow();
	private PersonWindow perw = new PersonWindow();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	/**
	 * Create the frame.
	 */
	public GUI() {
		try{
			FileInputStream fin = new FileInputStream("clients.ser");
			ObjectInputStream in = new ObjectInputStream(fin);
			//Hashtable<String,Person> myClients=(Hashtable<String,Person>)in.readObject();
			b.setClients((Hashtable<String,Person>)in.readObject());
		}catch(IOException i){
			i.printStackTrace();
		}catch(ClassNotFoundException c){
			c.printStackTrace();
	        return;
		}
		for(String s:b.getClients().keySet()){
			for(int j=0;j<b.getClients().get(s).getAccounts().size();j++){
				b.getClients().get(s).getAccounts().get(j).addObserver(b.getClients().get(s));
			}
		}
		//String s=new String();
		Object[] columns1={"personName","nrOfAccounts","moneyDeposited"};
		//Person p;
		Object[] row=new Object[3];
		int sum=0;
		Object[] row1=new Object[4];
		setTitle("BANK");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 783, 537);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		DefaultTableModel model1=new DefaultTableModel();
		
		//DefaultTableModel model1=new DefaultTableModel();
		model1.setColumnIdentifiers(columns1);
		Font font = new Font("",1,10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(181, 45, 404, 101);
		contentPane.add(scrollPane);
		JTable personsTable= new JTable();
		scrollPane.setViewportView(personsTable);
		//scrollPane.setViewportView(personsTable);
		personsTable.setModel(model1);
		personsTable.setVisible(true);
		personsTable.setBackground(Color.YELLOW);
		personsTable.setForeground(Color.BLACK);
		personsTable.setFont(font);
		for(String s: b.getClients().keySet()){
			p=b.getClients().get(s);
			row[0]=p.getName();
			row[1]=p.getAccounts().size();
			for(int i=0;i<p.getAccounts().size();i++){
				sum+=p.getAccounts().get(i).getMoney();
			}
			row[2]=sum;
			sum=0;
			model1.addRow(row);
		}
		//	clientTable.setRowHeight(20);
		personsTable.setAutoResizeMode(MAXIMIZED_BOTH);
		
		DefaultTableModel model2=new DefaultTableModel();
		Object[] columns2={"personName","accountId","moneyDeposited","accountType"};
		model2.setColumnIdentifiers(columns2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(181, 195, 404, 101);
		contentPane.add(scrollPane_1);
		JTable accountsTable= new JTable();
		scrollPane_1.setViewportView(accountsTable);
		accountsTable.setModel(model2);
		accountsTable.setVisible(true);
		accountsTable.setBackground(Color.YELLOW);
		accountsTable.setForeground(Color.BLACK);
		accountsTable.setFont(font);
		for(String s:b.getClients().keySet()){
			p=b.getClients().get(s);
			for(int i=0;i<p.getAccounts().size();i++){
				row1[0]=p.getName();
				row1[1]=p.getAccounts().get(i).getId();
				row1[2]=p.getAccounts().get(i).getMoney();
				row1[3]=p.getAccounts().get(i).getType() == 1 ? "Spending" : "Saving";
				model2.addRow(row1);
			}
		}
		accountsTable.setAutoResizeMode(MAXIMIZED_BOTH);
		
		JButton btnNewButton = new JButton("ADD PERSON");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int sem=0;
				String s1=textField_1.getText();
				for(String s:b.getClients().keySet()){
					if(s1.equals(s)){
						sem=1;
						break;
					}
				}
				if(sem==1){
					textField_3.setText("PERSON ALREADY EXISTS!!!");
				}else{
					textField_3.setText("PERSON ADDED SUCCESSFULLY!!!");
					
					String s=textField_1.getText();
					Person p = new Person(s);
					b.addPerson(p);
					int sum=0;
					row[0]=b.getClients().get(s).getName();
					row[1]=b.getClients().get(s).getAccounts().size();
					for(int i=0;i<p.getAccounts().size();i++){
						sum+=b.getClients().get(s).getAccounts().get(i).getMoney();
					}
					row[2]=sum;
					model1.addRow(row);
					perw.setBank(b);
					//accw.setModel(model2);
					perw.setTable();
					perw.setVisible(true);
				}
				
			}
		});
		btnNewButton.setBounds(24, 11, 147, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("DELETE PERSON");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int sem=0;
				for(String s:b.getClients().keySet()){
					if(s==personName){
						sem=1;
					}
				}
				if(sem==0){
					textField_3.setText("PERSON DOESN'T EXIST!!!");
				}else{
					b.removePerson(b.getClients().get(personName));
					textField_3.setText("PERSON DELETED SUCCESSFULLY!!!");
					
					int sum=0;
					for(int i=model1.getRowCount()-1;i>=0;i--)
						model1.removeRow(i);
					for(String s: b.getClients().keySet()){
						//this.p=
						row[0]=b.getClients().get(s).getName();//p.getName();
						row[1]=b.getClients().get(s).getAccounts().size();
						for(int i=0;i<b.getClients().get(s).getAccounts().size();i++){
							sum+=b.getClients().get(s).getAccounts().get(i).getMoney();
						}
						row[2]=sum;
						model1.addRow(row);
					}
					for(int i=model2.getRowCount()-1;i>=0;i--)
						model2.removeRow(i);
					for(String s:b.getClients().keySet()){
						p=b.getClients().get(s);
						for(int i=0;i<p.getAccounts().size();i++){
							row1[0]=p.getName();
							row1[1]=p.getAccounts().get(i).getId();
							row1[2]=p.getAccounts().get(i).getMoney();
							row1[3]=p.getAccounts().get(i).getType() == 1 ? "Spending" : "Saving";
							model2.addRow(row1);
						}
					}
					perw.setBank(b);
					//accw.setModel(model2);
					perw.setTable();
					perw.setVisible(true);
				}
			}
		});
		btnNewButton_1.setBounds(181, 11, 129, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("ADD ACCOUNT");
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				p=b.getClients().get(personName);
				if(!textField_2.getText().equalsIgnoreCase("Saving") && !textField_2.getText().equalsIgnoreCase("Spending"))
					textField_3.setText("INVALID ACCOUNT TYPE!!!");
				else{
					textField_3.setText("ACCOUNT ADDED SUCCESSFULLY!!!");
					if(textField_2.getText().equalsIgnoreCase("Saving")){
						Account a = new SavingAccount();
						a.addObserver(p);
						b.addAccount(p,a);
						
					}else{
						if(textField_2.getText().equalsIgnoreCase("Spending")){
							Account a = new SpendingAccount();
							a.addObserver(p);
							b.addAccount(p,a);
						}
					}
					int sum=0;
					for(int i=model1.getRowCount()-1;i>=0;i--)
						model1.removeRow(i);
					for(String s: b.getClients().keySet()){
						//this.p=
						row[0]=b.getClients().get(s).getName();//p.getName();
						row[1]=b.getClients().get(s).getAccounts().size();
						for(int i=0;i<b.getClients().get(s).getAccounts().size();i++){
							sum+=b.getClients().get(s).getAccounts().get(i).getMoney();
						}
						row[2]=sum;
						sum=0;
						model1.addRow(row);
					}
					for(int i=model2.getRowCount()-1;i>=0;i--)
						model2.removeRow(i);
					for(String s:b.getClients().keySet()){
						p=b.getClients().get(s);
						for(int i=0;i<p.getAccounts().size();i++){
							row1[0]=p.getName();
							row1[1]=p.getAccounts().get(i).getId();
							row1[2]=p.getAccounts().get(i).getMoney();
							row1[3]=p.getAccounts().get(i).getType() == 1 ? "Spending" : "Saving";
							model2.addRow(row1);
						}
					}
				}
				accw.setBank(b);
				//accw.setModel(model2);
				accw.setTable();
				accw.setVisible(true);
			}
		});
		btnNewButton_2.setBounds(21, 76, 150, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnDeleteAccount = new JButton("DELETE ACCOUNT");
		btnDeleteAccount.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(accountId==-1)
					textField_3.setText("SELECT AN ACCOUNT FOR DELETION!!");
				else{
					p=b.getClients().get(personName);
					textField_3.setText("ACCOUNT DELETED SUCCESSFULLY!!!");
					//Account a = p.getAccounts().get(index)
					int iaux=0;
					for(int i=0;i<p.getAccounts().size();i++)
						if(p.getAccounts().get(i).getId()==accountId)
							iaux=i;
					b.removeAccount(p,p.getAccounts().get(iaux));
					int sum=0;
					for(int i=model1.getRowCount()-1;i>=0;i--)
						model1.removeRow(i);
					for(String s: b.getClients().keySet()){
						//this.p=
						row[0]=b.getClients().get(s).getName();//p.getName();
						row[1]=b.getClients().get(s).getAccounts().size();
						for(int i=0;i<b.getClients().get(s).getAccounts().size();i++){
							sum+=b.getClients().get(s).getAccounts().get(i).getMoney();
						}
						row[2]=sum;
						sum=0;
						model1.addRow(row);
					}
					for(int i=model2.getRowCount()-1;i>=0;i--)
						model2.removeRow(i);
					for(String s:b.getClients().keySet()){
						p=b.getClients().get(s);
						for(int i=0;i<p.getAccounts().size();i++){
							row1[0]=p.getName();
							row1[1]=p.getAccounts().get(i).getId();
							row1[2]=p.getAccounts().get(i).getMoney();
							row1[3]=p.getAccounts().get(i).getType() == 1 ? "Spending" : "Saving";
							model2.addRow(row1);
						}
					}
					accw.setBank(b);
					//accw.setModel(model2);
					accw.setTable();
					accw.setVisible(true);
				}
			}});
		btnDeleteAccount.setBounds(24, 224, 147, 23);
		contentPane.add(btnDeleteAccount);
		
		JButton btnNewButton_3 = new JButton("ADD MONEY");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(accountId!=-1){
					int iaux=0;
					Person p=b.getClients().get(personName);
					for(int i=0;i<p.getAccounts().size();i++)
						if(p.getAccounts().get(i).getId()==accountId)
							iaux=i;
					b.getClients().get(personName).getAccounts().get(iaux).addMoney(Integer.parseInt(textField.getText()));
					textField_3.setText(personName+"  ADDED "+Integer.parseInt(textField.getText())+" EURO INTO ACCOUNT: "+b.getClients().get(personName).getAccounts().get(iaux).getId());
				//	textField_3.setText(Integer.toString(b.getClients().get(personName).getAccounts().get(iaux).getMoney()));

						//textField_3.setText("MAMA");
					int sum=0;
					for(int i=model1.getRowCount()-1;i>=0;i--)
						model1.removeRow(i);
					for(String s: b.getClients().keySet()){
						//this.p=
						row[0]=b.getClients().get(s).getName();//p.getName();
						row[1]=b.getClients().get(s).getAccounts().size();
						for(int i=0;i<b.getClients().get(s).getAccounts().size();i++){
							sum+=b.getClients().get(s).getAccounts().get(i).getMoney();
						}
						row[2]=sum;
						sum=0;
						model1.addRow(row);
					}
					for(int i=model2.getRowCount()-1;i>=0;i--)
						model2.removeRow(i);
					for(String s:b.getClients().keySet()){
						p=b.getClients().get(s);
						for(int i=0;i<p.getAccounts().size();i++){
							row1[0]=p.getName();
							row1[1]=p.getAccounts().get(i).getId();
							row1[2]=p.getAccounts().get(i).getMoney();
							row1[3]=p.getAccounts().get(i).getType() == 1 ? "Spending" : "Saving";
							model2.addRow(row1);
						}
					}
					accw.setBank(b);
					//accw.setModel(model2);
					accw.setTable();
					accw.setVisible(true);
				}
				
			}});
		
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_3.setBounds(24, 159, 147, 23);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("WITHDRAW MONEY");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(accountId!=-1){
					int iaux=0;
					Person p=b.getClients().get(personName);
					for(int i=0;i<p.getAccounts().size();i++)
						if(p.getAccounts().get(i).getId()==accountId)
							iaux=i;
					
					if(b.getClients().get(personName).getAccounts().get(iaux).getType()==0){
						textField_3.setText(personName+"  WITHDREW "+(b.getClients().get(personName).getAccounts().get(iaux).getMoney()+b.getClients().get(personName).getAccounts().get(iaux).getMoney()*8/100)+" FROM ACCOUNT: "+b.getClients().get(personName).getAccounts().get(iaux).getId());
						
						b.getClients().get(personName).getAccounts().get(iaux).withdrawMoney();
					}else{
						if(b.getClients().get(personName).getAccounts().get(iaux).getMoney()>=Integer.parseInt(textField.getText())){		
							textField_3.setText(personName+"  WITHDREW "+textField.getText()+" EURO FROM ACCOUNT: "+b.getClients().get(personName).getAccounts().get(iaux).getId());
							
							b.getClients().get(personName).getAccounts().get(iaux).withdrawMoney(Integer.parseInt(textField.getText()));
						}else
							textField_3.setText("INSUFFICIENT FUNDS!!!");
					}
						//textField_3.setText(Integer.toString(b.getClients().get(personName).getAccounts().get(iaux).getMoney()));

						//textField_3.setText("MAMA");
					int sum=0;
					for(int i=model1.getRowCount()-1;i>=0;i--)
						model1.removeRow(i);
					for(String s: b.getClients().keySet()){
						//this.p=
						row[0]=b.getClients().get(s).getName();//p.getName();
						row[1]=b.getClients().get(s).getAccounts().size();
						for(int i=0;i<b.getClients().get(s).getAccounts().size();i++){
							sum+=b.getClients().get(s).getAccounts().get(i).getMoney();
						}
						row[2]=sum;
						sum=0;
						model1.addRow(row);
					}
					for(int i=model2.getRowCount()-1;i>=0;i--)
						model2.removeRow(i);
					for(String s:b.getClients().keySet()){
						p=b.getClients().get(s);
						for(int i=0;i<p.getAccounts().size();i++){
							row1[0]=p.getName();
							row1[1]=p.getAccounts().get(i).getId();
							row1[2]=p.getAccounts().get(i).getMoney();
							row1[3]=p.getAccounts().get(i).getType() == 1 ? "Spending" : "Saving";
							model2.addRow(row1);
						}
					}
					accw.setBank(b);
					//accw.setModel(model2);
					accw.setTable();
					accw.setVisible(true);
				}
			}});
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNewButton_4.setBounds(181, 159, 129, 23);
		contentPane.add(btnNewButton_4);
		
		textField = new JTextField();
		textField.setBounds(64, 193, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblSum = new JLabel("SUM:");
		lblSum.setHorizontalAlignment(SwingConstants.CENTER);
		lblSum.setBounds(24, 196, 46, 14);
		contentPane.add(lblSum);
		
		textField_1 = new JTextField();
		textField_1.setBounds(63, 45, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblName = new JLabel("NAME:");
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setBounds(21, 48, 46, 14);
		contentPane.add(lblName);
		
		textField_2 = new JTextField();
		textField_2.setBounds(63, 110, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblType = new JLabel("TYPE:");
		lblType.setHorizontalAlignment(SwingConstants.CENTER);
		lblType.setBounds(23, 113, 46, 14);
		contentPane.add(lblType);
		
		JLabel lblAccounts = new JLabel("ACCOUNTS");
		lblAccounts.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAccounts.setHorizontalAlignment(SwingConstants.CENTER);
		lblAccounts.setBounds(456, 174, 129, 23);
		contentPane.add(lblAccounts);
		
		JLabel lblNewLabel = new JLabel("PERSONS");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(456, 29, 129, 14);
		contentPane.add(lblNewLabel);
		
		textField_3 = new JTextField();
		textField_3.setHorizontalAlignment(SwingConstants.CENTER);
		textField_3.setEditable(false);
		textField_3.setBounds(181, 359, 404, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblnotification = new JLabel("!!!NOTIFICATION!!!");
		lblnotification.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblnotification.setHorizontalAlignment(SwingConstants.CENTER);
		lblnotification.setBounds(283, 333, 197, 23);
		contentPane.add(lblnotification);
		
		//JButton btnNewButton_1 = new JButton("DELETE PERSON");
		personsTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				int row=personsTable.getSelectedRow();
				personName=personsTable.getValueAt(row,0).toString();
				accountId=-1;
				//nrOfAccounts=Integer.parseInt(personsTable.getValueAt(row,1).toString());
							}
		});
		//accountId=Integer.parseInt(personsTable.getValueAt(row, 1).toString());
		accountsTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				int row=accountsTable.getSelectedRow();
				personName=accountsTable.getValueAt(row,0).toString();
				accountId=Integer.parseInt(accountsTable.getValueAt(row, 1).toString());
				//nrOfAccounts=Integer.parseInt(personsTable.getValueAt(row,1).toString());
							}

		});
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				//ba.serializeBank();
				try{
					FileOutputStream fout= new FileOutputStream("clients.ser");
					ObjectOutputStream out = new ObjectOutputStream(fout);
					out.writeObject(b.getClients());
					out.close();
					fout.close();
				}catch(IOException i){
					i.printStackTrace();
				}
				System.exit(0);
			}

		});
	}
}
