package bank;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class PersonWindow extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Bank b=new Bank();
	private DefaultTableModel model1= new DefaultTableModel();
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the dialog.
	 */
	public PersonWindow() {
		setBounds(100, 100, 504, 230);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		Object[] columns1={"personName","nrOfAccounts","moneyDeposited"};
		//DefaultTableModel model1=new DefaultTableModel();
		
		//DefaultTableModel model1=new DefaultTableModel();
		model1.setColumnIdentifiers(columns1);
		Font font = new Font("",1,10);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 11, 452, 138);
		contentPanel.add(scrollPane);
		JTable personsTable= new JTable();
		scrollPane.setViewportView(personsTable);
		//scrollPane.setViewportView(personsTable);
		personsTable.setModel(model1);
		personsTable.setVisible(true);
		personsTable.setBackground(Color.YELLOW);
		personsTable.setForeground(Color.BLACK);
		personsTable.setFont(font);
		
	}
	public void setBank(Bank b){
		this.b=b;
	}
	//public void setModel(DefaultTableModel model2){
	//	this.model2=model2;
	//}
	public void setTable(){
		int sum=0;
		Object[] row = new Object[3];
		for(int i=model1.getRowCount()-1;i>=0;i--)
			model1.removeRow(i);
		for(String s: b.getClients().keySet()){
			//this.p=
			row[0]=b.getClients().get(s).getName();//p.getName();
			row[1]=b.getClients().get(s).getAccounts().size();
			for(int i=0;i<b.getClients().get(s).getAccounts().size();i++){
				sum+=b.getClients().get(s).getAccounts().get(i).getMoney();
			}
			row[2]=sum;
			sum=0;
			model1.addRow(row);
		}
	}
}
