package bank;
import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable{
	private int money=0;
	private static int idCnt=0;
	private int id=0;
	private int type;
	
	public Account(int type){
		this.idCnt+=1;
		this.id=idCnt;
		this.type=type;
	}
	//returns the money found in the account
	public int getMoney(){
		return this.money;
	}
	//sets the money in an account
	public void setMoney(int money){
		this.money=money;
	}
	//adds money to an account
	public void addMoney(int money){
		this.money+=money;
	}
	//withdraws money from an acoount
	public void withdrawMoney(int money){
		this.money-=money;
	}
	//gets the type of an account
	public int getType(){
		return this.type;
	}
	//withdraws all the money from an account
	public void withdrawMoney() {
		
	}
	//returns the id of an account
	public int getId(){
		return this.id;
	}
}
