package bank;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class AccountWindow extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Bank b=new Bank();
	private DefaultTableModel model2=new DefaultTableModel();
	/**
	 * Launch the application.
	 */

	/**
	 * Create the dialog.
	 */
	public AccountWindow() {
		setBounds(100, 100, 439, 199);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		//DefaultTableModel model2=new DefaultTableModel();
		Object[] columns2={"personName","accountId","moneyDeposited","accountType"};
		model2.setColumnIdentifiers(columns2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 404, 101);
		Font font = new Font("",1,10);
		
		contentPanel.add(scrollPane_1);
		JTable accountsTable= new JTable();
		scrollPane_1.setViewportView(accountsTable);
		accountsTable.setModel(model2);
		accountsTable.setVisible(true);
		accountsTable.setBackground(Color.YELLOW);
		accountsTable.setForeground(Color.BLACK);
		accountsTable.setFont(font);
		
	}
	public void setBank(Bank b){
		this.b=b;
	}
	public void setModel(DefaultTableModel model2){
		this.model2=model2;
	}
	public void setTable(){
		Person p;
		Object[] row1=new Object[4];
		for(int i=model2.getRowCount()-1;i>=0;i--)
			model2.removeRow(i);
		for(String s:b.getClients().keySet()){
			p=b.getClients().get(s);
			for(int i=0;i<p.getAccounts().size();i++){
				row1[0]=p.getName();
				row1[1]=p.getAccounts().get(i).getId();
				row1[2]=p.getAccounts().get(i).getMoney();
				row1[3]=p.getAccounts().get(i).getType() == 1 ? "Spending" : "Saving";
				model2.addRow(row1);
			}
		}
	}
}
